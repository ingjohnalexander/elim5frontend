import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from "./layout.component";
import { PageModule } from "./../page/page.module";

const routes: Routes = [

  {
    path: '',
    component: LayoutComponent,
    
    children: [
      {
        path: '',
        loadChildren: ()=> PageModule
      },       
      
     ]
     
  }
]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
