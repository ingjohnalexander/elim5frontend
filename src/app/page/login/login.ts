import { Component, OnInit } from '@angular/core';
import {Services}from './../../services/services';
import Swal from 'sweetalert2'
import { HttpClient, HttpResponse ,HttpHeaders,HttpParams} from '@angular/common/http';

declare const $:any;
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.html',
  providers: [Services],  
})
export class Login implements OnInit {
  loading=false;
  title="";
  url="api/login";
  form: FormGroup;
  
  in={
    username:"",email:"",password:"123"
  }

  constructor(private http:HttpClient,private _service: Services,public fb: FormBuilder) { 
    this.form = this.fb.group({
      'username': ['', [Validators.required]],
      'email': ['', [Validators.required,Validators.email]],
  });       
  }

  ngOnInit(): void {}

  onSubmit(){
    console.log(this.url);
    this._service.postAnonimous(this.url,this.in)
      .subscribe(data => {
        if(data.success=='OK'){
          localStorage.setItem('username',this.in.username);          
          window.open("?token="+data.token);
          this._service.addToast('success',data.mensaje,4000);
        }else{
          this._service.addToast('danger',data.mensaje,4000);
        }
      });
  }

}
