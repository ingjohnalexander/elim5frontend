import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { PageRoutingModule } from './page-routing.module';
import { StartComponent } from './start.component';

import {  Login } from './login/login';
import {  Registro } from './registro/registro';

import {  Start } from './start/start';

import {  Demo } from './demo/demo';


@NgModule({
  declarations: [StartComponent,
    Login,Start,Registro,Demo
  ],
  imports: [
    CommonModule,
    PageRoutingModule,
    FormsModule,ReactiveFormsModule
  ],
  providers: []
})
export class PageModule { }