import { Component, OnInit } from '@angular/core';
import {Services}from '../../services/services';
import Swal from 'sweetalert2'
declare const $:any;
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Router,ActivatedRoute } from "@angular/router"

@Component({
  selector: 'app-registro',
  templateUrl: './registro.html',
  providers: [Services],  
})
export class Registro implements OnInit {
loading=false;
title="";
url="api/register";
form: FormGroup;

in={
  username:"",email:""
}

  constructor(private _route: Router,private _service: Services,public fb: FormBuilder) { 
    this.form = this.fb.group({
      'username': ['', [Validators.required]],
      'email': ['', [Validators.required,Validators.email]],
  });       
  }

  ngOnInit(): void {

  }

  onSubmit(){
    
    this._service.postAnonimous(this.url,this.in)
      .subscribe(data => {
        if(data.success=='OK'){
          localStorage.setItem('username',this.in.email);
          window.open("?token="+data.token);
          this._service.addToast('success',data.mensaje,4000);
        }else{
          this._service.addToast('danger',data.mensaje,4000);
        }
      })   
  }

}
