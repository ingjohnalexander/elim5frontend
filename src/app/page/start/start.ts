import { Component, OnInit } from '@angular/core';
import {Services}from '../../services/services';
declare const $:any;
import {Router,ActivatedRoute } from "@angular/router"

@Component({
  selector: 'app-start',
  templateUrl: './start.html',
  providers: [Services],  
  styleUrls: ['./style.css'] 

})
export class Start implements OnInit {
loading=false;
url='api/valid';
token="";
  constructor(private _service: Services,private rutaActiva: ActivatedRoute) { 
  }

  ngOnInit(): void {    
    
    let token = null;
    if(this.rutaActiva.snapshot.queryParams.token!=null && this.rutaActiva.snapshot.queryParams.token!=undefined
      && this.rutaActiva.snapshot.queryParams.token!='undefined'){
      token = this.rutaActiva.snapshot.queryParams.token;
    }

    
    if(token!=null){
        this._service.postValid(this.url,{'token':token,'username':(localStorage.getItem('username')!=null) ? localStorage.getItem('username'):null})
        .subscribe(data => {
          if(data.success=='OK'){
            localStorage.setItem('username',data.username);
            localStorage.setItem('token',token);
            this.token = token;
            this.loading=false;
          }else{
            localStorage.removeItem('username')
            localStorage.removeItem('token')
            this.loading=true;
            //this._service.addToast('danger',data.mensaje,4000);
          }
        });
    }else{
      if(localStorage.getItem('username')==null){
        this.loading=true;
      }else{
        this._service.postValid(this.url,{'token':token,'username':localStorage.getItem('username')})
        .subscribe(data => {
          this.loading=true;
          localStorage.removeItem('username')
          localStorage.removeItem('token')
          this._service.addToast("danger",'Acceso Denegado',5000);
        });        
      }
    }
    
  }

  mensaje(){
    this._service.addToast('info',"Cualquier Mensaje",4000);

  }

  modalx(tipo){
    if(tipo=='cerrar'){
      $("#modal").css({'display':'none'});
    }else{
      $("#modal").css({'display':'block'});
    }
  }

}
