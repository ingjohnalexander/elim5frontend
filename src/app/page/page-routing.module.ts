import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartComponent } from './start.component';

import {  Login } from './login/login';
import {  Registro } from './registro/registro';

import {  Start } from './start/start';
import {  Demo } from './demo/demo';


const routes: Routes = [

  {
    path: '',
    component: StartComponent,
    children: [
      {path: '',component: Start },
      {path: 'demo1',component: Demo },
      {path: 'demo2',component: Demo },
      {path: 'demo3',component: Demo },
      {path: 'login',component: Login },
      {path: 'registro',component: Registro },
     ]
     
  }
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }
