import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse ,HttpHeaders,HttpParams} from '@angular/common/http';
import { Location } from '@angular/common'; 
import Swal from 'sweetalert2'
import { Observable, throwError } from 'rxjs';
declare var require: any;
declare const $:any;
import { retry, catchError } from 'rxjs/operators';


@Injectable()
export class Services {
//url="http://localhost/pruebaelim5/public/index.php/";
url="http://pruebaelim5.jhonchapid.com/backend/public/index.php/";
    constructor(public location: Location,private http:HttpClient) { 
    }

/* Me permite retornar las cabeceras para envio de datos*/
getHeaders(estado){
    let options;
    if(estado=="SI"){
         options = {headers: new HttpHeaders().append('Authorization', 'Bearer '+ localStorage.getItem('token')).append('Content-Type', 'application/json') }   
    }else{
        options = {};
    }
    return options;    
}


 // Http Options
 httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  } 





    // HttpClient API post() method => Create employee
    postAnonimous(ruta,json): Observable<any> {
        return this.http.post(this.url + ruta, JSON.stringify(json), this.httpOptions)
        .pipe(
          retry(1),
          catchError(this.handleError)
        )
      }  

    // HttpClient API post() method => Create employee
    postValid(ruta,json): Observable<any> {
      let httpOptionsToken = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization':'Bearer '+ json.token
        })
      } 

      return this.http.post(this.url + ruta, json, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
    }  

  // Error handling 
  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    //window.alert(errorMessage);
    return throwError(errorMessage);
 }













/**
 * Funcion que elimina un registro con confirmacion
 */
goBack() {
this.location.back();
}    

/**
 * Funcion: Permite realizar cualqueir notificacion para que sea vista en pantalla y el tipo de mensaje y su mensaje
 */
addToast(type,msg,tiempo=null) { 
    if(tiempo==null){ tiempo=1500;  } Swal.fire({position: 'top-end',icon: type,title: msg,timer: 1500})
}


}